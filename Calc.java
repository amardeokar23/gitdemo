public class Calc {

            public int add(int no1, int no2) {
                  return no1+no2;
            }
            public int add(String n1, String n2) {
                  int no1 = Integer.parseInt(n1);
                  int no2 = Integer.parseInt(n2);
                  return no1 + no2;
            }
            public int divide(String n1, String n2) {
                  int no1 = Integer.parseInt(n1);
                  int no2 = Integer.parseInt(n2);
                  return no1 / no2;
            }
            public static void main(String[] args) {
                  Calc c1 = new Calc();
                  System.out.println("add of 10, 10 is " + c1.add(10,10));
                  System.out.println("add of '15', '10' is " + c1.add("15","10"));
                  //System.out.println("add of '15', 'a' is " + c1.add("15","a"));
                  System.out.println("divide of '10', '2' is " + c1.divide("10","2"));
            //	System.out.println("divide of '10', 'a' is " + c1.divide("10","a"));
                  //System.out.println("divide of '10', '0' is " + c1.divide("10","0"));
            }
      }
